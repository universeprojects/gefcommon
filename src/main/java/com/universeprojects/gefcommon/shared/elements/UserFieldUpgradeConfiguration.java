package com.universeprojects.gefcommon.shared.elements;

public interface UserFieldUpgradeConfiguration {
    String getFieldName();
    String getName();
    Double getRarityValue();
    String getRarity();
}
