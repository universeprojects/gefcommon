package com.universeprojects.gefcommon.shared.elements;

public interface QuestObjectiveTutorialElement {

    String getArrowUIID();

    String getPopupIcon();

    String getPopupText();

    String getDialogId();

    String getPopupAudio();

    String getDoneCheckerHasSkill();

    String getDoneCheckerHasItemInInventory();

    String getDoneCheckerHasItemNearby();

    String getDoneCheckerDialogVisible();

    String getDoneCheckerUIComponentVisible();

    String getDoneCheckerTriggerType();

}
