package com.universeprojects.gefcommon.shared.elements;

public interface Skill {
    Long getId();

    Long getIdeaId();

    String getName();

    String getDescription();

    String getIcon();

    Long getBaseTimeToRunSecs();

    Double getBaseSuccessRate();

    GameObject<String> getGameObject();

    SkillType getSkillType();

    Long getParentCategoryId();
}
