package com.universeprojects.gefcommon.shared.elements;

import java.util.Collection;
import java.util.Map;
import java.util.List;
public interface RecipeConstruction {
    Collection<? extends RecipeConstructionSlot> getSlotData();
    Map<String, String> getUserFieldValues();
    Map<String, Double> getUserFieldDistributions();
    List<String> getFieldsToUpgrade();
    Long getImprovementSkillId();
}
