package com.universeprojects.gefcommon.shared.elements;

@SuppressWarnings("rawtypes")
public interface RecipeConstructionSlot {
    String getSlotId();

    GameObject getObject();
}
