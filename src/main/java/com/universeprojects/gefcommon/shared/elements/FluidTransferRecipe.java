package com.universeprojects.gefcommon.shared.elements;

import java.util.Map;

public interface FluidTransferRecipe {
    Map<String, Float> getSourceTypeRatios();
    String getTargetType();
    boolean canReverse();
}
