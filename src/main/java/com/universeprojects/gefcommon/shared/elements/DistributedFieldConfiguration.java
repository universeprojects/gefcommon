package com.universeprojects.gefcommon.shared.elements;

public interface DistributedFieldConfiguration {
    String getFieldName();
    String getDisplayName();
    String getDescription();
    String getIcon();
    Double getMaxPoints();
    Double getBaseMinValue();
    Double getBaseMaxValue();
    Double getMultiplierMinValue();
    Double getMultiplierMaxValue();
}
